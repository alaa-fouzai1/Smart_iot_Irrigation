export class Sensor {
  public id: string;
  public Name: string;
  public SensorType: string;
  public Description: string;
  public SensorCoordinates: [];
  public createdate: Date;
  public data: [];
}
